import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/repository/database/user_database.dart';
import 'package:majootestcase/repository/movie_repository.dart';
import 'package:majootestcase/repository/network/dio_config_service.dart';
import 'package:majootestcase/repository/network/movie_service.dart';
import 'package:majootestcase/repository/user_repository.dart';
import 'package:majootestcase/ui/detail/movie_detail_page.dart';
import 'package:majootestcase/ui/home/home_page.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/ui/register/register_page.dart';

class AppModule extends Module {
  @override
  List<Bind<Object>> get binds => [
        //Factory
        Bind.factory((i) => UserDatabase()),
        Bind.factory((i) => MovieService(i())),

        //Singleton
        Bind.singleton((i) => Dio(options)..interceptors.add(interceptor)),
        Bind.singleton((i) => UserRepository(i())),
        Bind.singleton((i) => MovieRepository(i())),
      ];

  @override
  List<ModularRoute> get routes => _routes;
}

final _routes = <ModularRoute>[
  MyHomePageScreen.route,
  HomePage.route,
  LoginPage.route,
  RegisterPage.route,
  MovieDetailPage.route,
];
