import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/repository/user_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  final UserRepository _userRepository;

  static AuthBlocCubit create(BuildContext context) =>
      AuthBlocCubit._(Modular.get())..fetchHistoryLogin();

  AuthBlocCubit._(this._userRepository) : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    String? loggedInUser = sharedPreferences.getString('user_value');
    if (isLoggedIn == null || !isLoggedIn) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn && loggedInUser != null) {
        final user = User.fromJson(jsonDecode(loggedInUser));
        emit(AuthBlocLoggedInState(user));
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(BuildContext context,
      {required String email, required String password}) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    try {
      emit(AuthBlocLoadingState());
      final user = await _userRepository.getUser('users',
          email: email, password: password);
      final loggedInUser = jsonEncode(user.toJson());
      await sharedPreferences.setBool("is_logged_in", true);
      await sharedPreferences.setString("user_value", loggedInUser);
      emit(AuthBlocLoggedInState(user));
    } on StateError catch (e) {
      emit(AuthBlocNoUserState(e));
    } catch (e) {
      emit(AuthBlocErrorState(e));
    }
  }

  void register(BuildContext context,
      {required String username,
      required String email,
      required String password}) async {
    try {
      emit(AuthBlocLoadingState());
      await _userRepository.insert('users',
          username: username, email: email, password: password);
      loginUser(context, email: email, password: password);
    } catch (e) {
      emit(AuthBlocErrorState(e));
    }
  }

  void logout() async {
    try {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      await sharedPreferences.setBool("is_logged_in", false);
      await sharedPreferences.remove("user_value");
      emit(AuthBlocLoginState());
    } catch (e) {
      emit(AuthBlocErrorState(e));
    }
  }
}
