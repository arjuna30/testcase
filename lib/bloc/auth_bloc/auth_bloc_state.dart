part of 'auth_bloc_cubit.dart';

abstract class AuthBlocState {
  const AuthBlocState();
}

class AuthBlocInitialState extends AuthBlocState {}

class AuthBlocLoadingState extends AuthBlocState {}

class AuthBlocLoggedInState extends AuthBlocState {
  final User user;

  AuthBlocLoggedInState(this.user);
}

class AuthBlocLoginState extends AuthBlocState {}

class AuthBlocSuccesState extends AuthBlocState {}

class AuthBlocLoadedState extends AuthBlocState {
  final data;

  AuthBlocLoadedState(this.data);
}

class AuthBlocErrorState extends AuthBlocState {
  final error;

  AuthBlocErrorState(this.error);
}

class AuthBlocNoUserState extends AuthBlocState {
  final error;

  AuthBlocNoUserState(this.error);
}
