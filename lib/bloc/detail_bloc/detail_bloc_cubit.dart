import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:majootestcase/models/movie_detail_response.dart';
import 'package:majootestcase/repository/movie_repository.dart';

part 'detail_bloc_state.dart';

class DetailBlocCubit extends Cubit<DetailBlocState> {
  final MovieRepository _movieRepository;
  static DetailBlocCubit create(BuildContext context) =>
      DetailBlocCubit._(Modular.get());

  DetailBlocCubit._(this._movieRepository) : super(DetailBlocInitial());

  Future<void> fetchingDetail(String id) async {
    try {
      emit(DetailBlocLoadingState());
      MovieDetailResponse movieDetailResponse =
          await _movieRepository.getMovieOverviewDetail(id);
      emit(DetailBlocLoadedState(movieDetailResponse));
    } on DioError catch (e) {
      final iconData = Icons.signal_wifi_connected_no_internet_4;
      if (DioErrorType.receiveTimeout == e.type) {
        emit(DetailBlocErrorState("Receive Timeout", iconData));
        return;
      }
      if (DioErrorType.connectTimeout == e.type) {
        emit(DetailBlocErrorState("Connection Timeout", iconData));
        return;
      }
      emit(DetailBlocErrorState("No Internet Connection", iconData));
    } catch (e) {
      emit(DetailBlocErrorState('Something went wrong'));
    }
  }
}
