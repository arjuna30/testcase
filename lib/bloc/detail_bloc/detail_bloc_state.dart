part of 'detail_bloc_cubit.dart';

@immutable
abstract class DetailBlocState extends Equatable {
  const DetailBlocState();
  @override
  List<Object> get props => [];
}

class DetailBlocInitial extends DetailBlocState {}

class DetailBlocLoadingState extends DetailBlocState {}

class DetailBlocLoadedState extends DetailBlocState {
  final MovieDetailResponse movieDetailResponse;

  DetailBlocLoadedState(this.movieDetailResponse);

  @override
  List<Object> get props => [movieDetailResponse];
}

class DetailBlocErrorState extends DetailBlocState {
  final error;
  final IconData? iconData;

  DetailBlocErrorState(this.error, [this.iconData]);

  @override
  List<Object> get props => [error];
}
