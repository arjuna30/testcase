import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart' as modular;
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/repository/movie_repository.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  final MovieRepository _movieRepository;

  static HomeBlocCubit create(BuildContext context) =>
      HomeBlocCubit._(modular.Modular.get());

  HomeBlocCubit._(this._movieRepository) : super(HomeBlocInitialState());

  void fetchingData() async {
    try {
      emit(HomeBlocLoadingState());
      MovieResponse movieResponse = await _movieRepository.getMovieList();
      emit(HomeBlocLoadedState(movieResponse.data));
    } on DioError catch (e) {
      final iconData = Icons.signal_wifi_connected_no_internet_4;
      if (DioErrorType.receiveTimeout == e.type) {
        emit(HomeBlocErrorState("Receive Timeout", iconData));
        return;
      }
      if (DioErrorType.connectTimeout == e.type) {
        emit(HomeBlocErrorState("Connection Timeout", iconData));
        return;
      }
      emit(HomeBlocErrorState("No Internet Connection", iconData));
    } catch (e) {
      emit(HomeBlocErrorState("Something went wrong"));
    }
  }
}
