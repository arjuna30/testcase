import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class CustomInfoDialog extends StatefulWidget {
  final String text;

  const CustomInfoDialog({
    Key? key,
    required this.text,
  }) : super(key: key);

  static void showInfoDialog(
    BuildContext context,
    String text,
  ) =>
      showDialog(
          context: context, builder: (context) => CustomInfoDialog(text: text));

  @override
  _CustomInfoDialogState createState() => _CustomInfoDialogState();
}

class _CustomInfoDialogState extends State<CustomInfoDialog> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Modular.to.pop(),
      child: Dialog(
        backgroundColor: Colors.transparent,
        child: Container(
            height: 100,
            width: 100,
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: const [
                BoxShadow(
                  color: Colors.black38,
                  blurRadius: 5.0,
                  offset: Offset(0.0, 5.0),
                )
              ],
            ),
            child: Center(
                child: Text(
              widget.text,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ))),
      ),
    );
  }
}
