import 'package:flutter/material.dart';

class CustomLoadingDialog extends StatefulWidget {
  const CustomLoadingDialog({
    Key? key,
  }) : super(key: key);

  static void showInfoDialog(BuildContext context) => showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => CustomLoadingDialog());

  @override
  _CustomLoadingDialogState createState() => _CustomLoadingDialogState();
}

class _CustomLoadingDialogState extends State<CustomLoadingDialog> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Dialog(
        backgroundColor: Colors.transparent,
        child: Container(
          height: 150,
          width: 100,
          padding: EdgeInsets.all(20.0),
          decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: const [
              BoxShadow(
                color: Colors.black38,
                blurRadius: 10.0,
                offset: Offset(0.0, 10.0),
              )
            ],
          ),
          child: Center(child: CircularProgressIndicator()),
        ),
      ),
    );
  }
}
