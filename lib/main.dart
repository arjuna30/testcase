import 'package:majootestcase/app_module.dart';
import 'package:majootestcase/common/widget/dialog/custom_info_dialog.dart';
import 'package:majootestcase/common/widget/dialog/custom_loading_dialog.dart';
import 'package:majootestcase/ui/home/home_page.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/bloc/detail_bloc/detail_bloc_cubit.dart';

void main() => runApp(ModularApp(module: AppModule(), child: MyApp()));

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: AuthBlocCubit.create),
        BlocProvider(create: HomeBlocCubit.create),
        BlocProvider(create: DetailBlocCubit.create),
      ],
      child: MaterialApp(
          title: 'Majoo Test Case',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          )).modular(),
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  static final route = ChildRoute(Modular.initialRoute,
      child: (context, args) => MyHomePageScreen());

  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocLoadingState) {
            CustomLoadingDialog.showInfoDialog(context);
          }
          if (state is AuthBlocNoUserState) {
            Modular.to.pop();
            CustomInfoDialog.showInfoDialog(
                context, 'Login gagal, periksa email & password kembali');
          }
          if (state is AuthBlocLoggedInState) {
            Modular.to.pushReplacementNamed(HomePage.route.name);
          }
        },
        child: LoginPage());
  }
}
