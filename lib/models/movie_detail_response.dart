import 'package:json_annotation/json_annotation.dart';

part 'movie_detail_response.g.dart';

@JsonSerializable()
class MovieDetailResponse {
  final String id;
  final Title title;
  final Rating ratings;
  final List<String> genres;
  final String? releaseDate;
  final Plot? plotOutline;
  final Plot? plotSummary;

  const MovieDetailResponse(
      {required this.id,
      required this.title,
      required this.ratings,
      required this.genres,
      required this.releaseDate,
      required this.plotOutline,
      required this.plotSummary});

  factory MovieDetailResponse.fromJson(Map<String, dynamic> json) =>
      _$MovieDetailResponseFromJson(json);
  Map<String, dynamic> toJson() => _$MovieDetailResponseToJson(this);
}

@JsonSerializable()
class Title {
  @JsonKey(name: '@type')
  final String type;
  final String id;
  final I image;
  final num? runningTimeInMinutes;
  final String? nextEpisode;
  final int? numberOfEpisodes;
  final int? seriesEndYear;
  final int? seriesStartYear;
  final String title;
  final String titleType;
  final int year;

  const Title({
    required this.type,
    required this.id,
    required this.image,
    required this.runningTimeInMinutes,
    required this.nextEpisode,
    required this.numberOfEpisodes,
    required this.seriesEndYear,
    required this.seriesStartYear,
    required this.title,
    required this.titleType,
    required this.year,
  });

  factory Title.fromJson(Map<String, dynamic> json) => _$TitleFromJson(json);
  Map<String, dynamic> toJson() => _$TitleToJson(this);
}

@JsonSerializable()
class I {
  final int height;
  final String id;
  final String url;
  final int width;

  const I(
      {required this.height,
      required this.id,
      required this.url,
      required this.width});
  factory I.fromJson(Map<String, dynamic> json) => _$IFromJson(json);
  Map<String, dynamic> toJson() => _$IToJson(this);
}

@JsonSerializable()
class Rating {
  final bool canRate;
  final num rating;
  final int ratingCount;

  const Rating(
      {required this.canRate, required this.rating, required this.ratingCount});
  factory Rating.fromJson(Map<String, dynamic> json) => _$RatingFromJson(json);
  Map<String, dynamic> toJson() => _$RatingToJson(this);
}

@JsonSerializable()
class Plot {
  final String? author;
  final String id;
  final String text;

  const Plot({this.author, required this.id, required this.text});
  factory Plot.fromJson(Map<String, dynamic> json) => _$PlotFromJson(json);
  Map<String, dynamic> toJson() => _$PlotToJson(this);
}
