// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_detail_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MovieDetailResponse _$MovieDetailResponseFromJson(Map<String, dynamic> json) =>
    MovieDetailResponse(
      id: json['id'] as String,
      title: Title.fromJson(json['title'] as Map<String, dynamic>),
      ratings: Rating.fromJson(json['ratings'] as Map<String, dynamic>),
      genres:
          (json['genres'] as List<dynamic>).map((e) => e as String).toList(),
      releaseDate: json['releaseDate'] as String?,
      plotOutline: json['plotOutline'] == null
          ? null
          : Plot.fromJson(json['plotOutline'] as Map<String, dynamic>),
      plotSummary: json['plotSummary'] == null
          ? null
          : Plot.fromJson(json['plotSummary'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MovieDetailResponseToJson(
        MovieDetailResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'ratings': instance.ratings,
      'genres': instance.genres,
      'releaseDate': instance.releaseDate,
      'plotOutline': instance.plotOutline,
      'plotSummary': instance.plotSummary,
    };

Title _$TitleFromJson(Map<String, dynamic> json) => Title(
      type: json['@type'] as String,
      id: json['id'] as String,
      image: I.fromJson(json['image'] as Map<String, dynamic>),
      runningTimeInMinutes: json['runningTimeInMinutes'] as num?,
      nextEpisode: json['nextEpisode'] as String?,
      numberOfEpisodes: json['numberOfEpisodes'] as int?,
      seriesEndYear: json['seriesEndYear'] as int?,
      seriesStartYear: json['seriesStartYear'] as int?,
      title: json['title'] as String,
      titleType: json['titleType'] as String,
      year: json['year'] as int,
    );

Map<String, dynamic> _$TitleToJson(Title instance) => <String, dynamic>{
      '@type': instance.type,
      'id': instance.id,
      'image': instance.image,
      'runningTimeInMinutes': instance.runningTimeInMinutes,
      'nextEpisode': instance.nextEpisode,
      'numberOfEpisodes': instance.numberOfEpisodes,
      'seriesEndYear': instance.seriesEndYear,
      'seriesStartYear': instance.seriesStartYear,
      'title': instance.title,
      'titleType': instance.titleType,
      'year': instance.year,
    };

I _$IFromJson(Map<String, dynamic> json) => I(
      height: json['height'] as int,
      id: json['id'] as String,
      url: json['url'] as String,
      width: json['width'] as int,
    );

Map<String, dynamic> _$IToJson(I instance) => <String, dynamic>{
      'height': instance.height,
      'id': instance.id,
      'url': instance.url,
      'width': instance.width,
    };

Rating _$RatingFromJson(Map<String, dynamic> json) => Rating(
      canRate: json['canRate'] as bool,
      rating: json['rating'] as num,
      ratingCount: json['ratingCount'] as int,
    );

Map<String, dynamic> _$RatingToJson(Rating instance) => <String, dynamic>{
      'canRate': instance.canRate,
      'rating': instance.rating,
      'ratingCount': instance.ratingCount,
    };

Plot _$PlotFromJson(Map<String, dynamic> json) => Plot(
      author: json['author'] as String?,
      id: json['id'] as String,
      text: json['text'] as String,
    );

Map<String, dynamic> _$PlotToJson(Plot instance) => <String, dynamic>{
      'author': instance.author,
      'id': instance.id,
      'text': instance.text,
    };
