import 'package:json_annotation/json_annotation.dart';

part 'movie_response.g.dart';

@JsonSerializable()
class MovieResponse {
  @JsonKey(name: 'd')
  final List<Data> data;
  @JsonKey(name: 'q')
  final String query;
  final int v;

  const MovieResponse(
      {required this.data, required this.query, required this.v});

  factory MovieResponse.fromJson(Map<String, dynamic> json) =>
      _$MovieResponseFromJson(json);
  Map<String, dynamic> toJson() => _$MovieResponseToJson(this);
}

@JsonSerializable()
class Data {
  final I i;
  final String id;
  final String l;
  final String q;
  final int rank;
  final String s;
  @JsonKey(name: 'v')
  final List<Series>? series;
  final int? vt;
  @JsonKey(name: 'y')
  final int year;
  final String? yr;

  const Data(
      {required this.i,
      required this.id,
      required this.l,
      required this.q,
      required this.rank,
      required this.s,
      this.series,
      this.vt,
      required this.year,
      this.yr});

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class I {
  final int height;
  final String imageUrl;
  final int width;

  const I({required this.height, required this.imageUrl, required this.width});

  factory I.fromJson(Map<String, dynamic> json) => _$IFromJson(json);
  Map<String, dynamic> toJson() => _$IToJson(this);
}

@JsonSerializable()
class Series {
  final I i;
  final String id;
  final String l;
  final String s;

  const Series(
      {required this.i, required this.id, required this.l, required this.s});

  factory Series.fromJson(Map<String, dynamic> json) => _$SeriesFromJson(json);
  Map<String, dynamic> toJson() => _$SeriesToJson(this);
}
