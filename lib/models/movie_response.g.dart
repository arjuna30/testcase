// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MovieResponse _$MovieResponseFromJson(Map<String, dynamic> json) =>
    MovieResponse(
      data: (json['d'] as List<dynamic>)
          .map((e) => Data.fromJson(e as Map<String, dynamic>))
          .toList(),
      query: json['q'] as String,
      v: json['v'] as int,
    );

Map<String, dynamic> _$MovieResponseToJson(MovieResponse instance) =>
    <String, dynamic>{
      'd': instance.data,
      'q': instance.query,
      'v': instance.v,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      i: I.fromJson(json['i'] as Map<String, dynamic>),
      id: json['id'] as String,
      l: json['l'] as String,
      q: json['q'] as String,
      rank: json['rank'] as int,
      s: json['s'] as String,
      series: (json['v'] as List<dynamic>?)
          ?.map((e) => Series.fromJson(e as Map<String, dynamic>))
          .toList(),
      vt: json['vt'] as int?,
      year: json['y'] as int,
      yr: json['yr'] as String?,
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'i': instance.i,
      'id': instance.id,
      'l': instance.l,
      'q': instance.q,
      'rank': instance.rank,
      's': instance.s,
      'v': instance.series,
      'vt': instance.vt,
      'y': instance.year,
      'yr': instance.yr,
    };

I _$IFromJson(Map<String, dynamic> json) => I(
      height: json['height'] as int,
      imageUrl: json['imageUrl'] as String,
      width: json['width'] as int,
    );

Map<String, dynamic> _$IToJson(I instance) => <String, dynamic>{
      'height': instance.height,
      'imageUrl': instance.imageUrl,
      'width': instance.width,
    };

Series _$SeriesFromJson(Map<String, dynamic> json) => Series(
      i: I.fromJson(json['i'] as Map<String, dynamic>),
      id: json['id'] as String,
      l: json['l'] as String,
      s: json['s'] as String,
    );

Map<String, dynamic> _$SeriesToJson(Series instance) => <String, dynamic>{
      'i': instance.i,
      'id': instance.id,
      'l': instance.l,
      's': instance.s,
    };
