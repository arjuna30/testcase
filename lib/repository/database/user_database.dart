import 'package:flutter/material.dart';
import 'package:majootestcase/models/user.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class UserDatabase {
  static const String _table = 'users';
  static const String createUserTable =
      "CREATE TABLE IF NOT EXISTS $_table ( email TEXT , username TEXT , password TEXT ) ";

  Future<Database> _openDB() async {
    final dbPath = await getDatabasesPath();
    final db = await openDatabase(join(dbPath, 'test_case.db'),
        onCreate: (db, version) {
      db.execute(createUserTable);
    }, version: 1);
    return db;
  }

  Future<void> insert(String table,
      {required String username,
      required String email,
      required String password}) async {
    final db = await _openDB();
    final data = <String, dynamic>{
      'username': username,
      'email': email,
      'password': password,
    };
    await db.insert(table, data, conflictAlgorithm: ConflictAlgorithm.replace);
    debugPrint('Success insert to db');
  }

  Future<User> getUser(String table,
      {required String email, required String password}) async {
    final db = await _openDB();
    final result = await db.query(table,
        where: 'email = ? AND password = ?', whereArgs: [email, password]);
    return User.fromJson(result.first);
  }
}
