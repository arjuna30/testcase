import 'package:majootestcase/models/movie_detail_response.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/repository/network/movie_service.dart';

class MovieRepository {
  final MovieService _movieService;

  MovieRepository(this._movieService);

  Future<MovieResponse> getMovieList() async => _movieService.getMovieList();

  Future<MovieDetailResponse> getMovieOverviewDetail(String id) async =>
      _movieService.getMovieOverviewDetail(id);
}
