import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

var options = BaseOptions(
  baseUrl: "https://imdb8.p.rapidapi.com/",
  connectTimeout: 12000,
  receiveTimeout: 12000,
  headers: {
    // "x-rapidapi-key": "6af4f77398mshb3f6cd027729468p1d6d31jsn038947888a17",
    "x-rapidapi-key":
        "e02a376704mshe3a174d5b240954p1a92e4jsn0990f984e53b", // self key
    "x-rapidapi-host": "imdb8.p.rapidapi.com"
  },
);

var interceptor = PrettyDioLogger(
  requestHeader: true,
  requestBody: true,
  responseBody: true,
  responseHeader: false,
  error: true,
  compact: true,
  maxWidth: 90,
);
