import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_detail_response.dart';
import 'package:majootestcase/models/movie_response.dart';

class MovieService {
  final Dio _dio;
  const MovieService(this._dio);

  Future<MovieResponse> getMovieList() async {
    final path = 'auto-complete?q=game%20of%20thr';
    final response = await _dio.get(path);
    final movieResponse = MovieResponse.fromJson(response.data);
    return movieResponse;
  }

  Future<MovieDetailResponse> getMovieOverviewDetail(String id) async {
    final path = 'title/get-overview-details?tconst=$id';
    final response = await _dio.get(path);
    final movieDetailResponse = MovieDetailResponse.fromJson(response.data);
    return movieDetailResponse;
  }
}
