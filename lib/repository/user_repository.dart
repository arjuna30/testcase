import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/repository/database/user_database.dart';

class UserRepository {
  final UserDatabase _userDatabase;

  UserRepository(this._userDatabase);

  Future<User> getUser(String table,
          {required String email, required String password}) async =>
      await _userDatabase.getUser(table, email: email, password: password);

  Future<void> insert(String table,
          {required String username,
          required String email,
          required String password}) async =>
      _userDatabase.insert(table,
          username: username, email: email, password: password);
}
