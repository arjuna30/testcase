import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:majootestcase/bloc/detail_bloc/detail_bloc_cubit.dart';
import 'package:majootestcase/models/movie_detail_response.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/ui/extra/loading.dart';

class MovieDetailPage extends StatefulWidget {
  static final route = ChildRoute('/home/movie/:id',
      child: (context, args) => MovieDetailPage());

  const MovieDetailPage({Key? key}) : super(key: key);

  @override
  State<MovieDetailPage> createState() => _MovieDetailPageState();
}

class _MovieDetailPageState extends State<MovieDetailPage> {
  final id = Modular.args.params['id'];

  @override
  void initState() {
    super.initState();
    context.read<DetailBlocCubit>().fetchingDetail(id);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DetailBlocCubit, DetailBlocState>(
      builder: (context, state) {
        if (state is DetailBlocLoadingState) {
          return LoadingIndicator();
        }
        if (state is DetailBlocErrorState) {
          return ErrorScreen(
            message: '${state.error}',
            fontSize: 20,
            iconData: state.iconData,
            retryButton: IconButton(
              icon: Icon(Icons.refresh_sharp, size: 35),
              onPressed: () {
                context.read<DetailBlocCubit>().fetchingDetail(id);
              },
            ),
          );
        }
        if (state is DetailBlocLoadedState) {
          final movieDetailResponse = state.movieDetailResponse;
          return _BodyMovieDetailPage(movieDetailResponse: movieDetailResponse);
        }
        return Scaffold(
          body: Center(
              child: Text(kDebugMode ? "state not implemented $state" : "")),
        );
      },
    );
  }
}

class _BodyMovieDetailPage extends StatelessWidget {
  final MovieDetailResponse movieDetailResponse;
  const _BodyMovieDetailPage({Key? key, required this.movieDetailResponse})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final runningTimeInMinutes = movieDetailResponse.title.runningTimeInMinutes;
    final plot = movieDetailResponse.plotSummary?.text ??
        movieDetailResponse.plotOutline?.text;
    return Scaffold(
      body: CustomScrollView(
        shrinkWrap: true,
        physics: const ClampingScrollPhysics(),
        slivers: [
          SliverAppBar(
            expandedHeight: MediaQuery.of(context).size.height * 0.35,
            backgroundColor: Colors.transparent,
            flexibleSpace: FlexibleSpaceBar(
                background: Image.network(
              movieDetailResponse.title.image.url,
              fit: BoxFit.cover,
            )),
          ),
          SliverFixedExtentList(
              itemExtent: MediaQuery.of(context).size.height * 0.95,
              delegate: SliverChildListDelegate([
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Text(
                              movieDetailResponse.title.title,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold),
                            ),
                          ),
                          const SizedBox(width: 25),
                          Row(
                            children: [
                              const Text(
                                '★ ',
                                style: TextStyle(
                                    fontSize: 18, color: Colors.orangeAccent),
                              ),
                              Text(
                                '${movieDetailResponse.ratings.rating}',
                                style: const TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(height: 10),
                      Text('Released on ${movieDetailResponse.title.year}'),
                      const SizedBox(height: 10),
                      if (runningTimeInMinutes != null)
                        Text(_formatedTime(runningTimeInMinutes)),
                      if (runningTimeInMinutes != null)
                        const SizedBox(height: 15),
                      if (plot != null)
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(plot),
                              const SizedBox(height: 30),
                            ],
                          ),
                        ),
                    ],
                  ),
                ),
              ])),
        ],
      ),
    );
  }

  String _formatedTime(num minutes) {
    num hour = minutes ~/ 60;
    num min = minutes % 60;
    String parsedTime = hour.toString() + 'h ' + min.toString() + 'm';
    return parsedTime;
  }
}
