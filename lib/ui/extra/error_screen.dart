import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Color? textColor;
  final double fontSize;
  final double gap;
  final Widget? retryButton;
  final IconData? iconData;

  const ErrorScreen({
    Key? key,
    this.gap = 10,
    this.retryButton,
    this.message = "",
    this.fontSize = 14,
    this.textColor,
    this.iconData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (iconData != null) Icon(iconData, size: 50),
            if (iconData != null) SizedBox(height: 20),
            Text(
              message,
              style: TextStyle(
                  fontSize: fontSize, color: textColor ?? Colors.black),
            ),
            Column(
              children: [
                SizedBox(height: 100),
                retryButton ??
                    IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.refresh_sharp),
                    ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
