import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart' as modular;
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/login/login_page.dart';

class HomePage extends StatefulWidget {
  static final route =
      modular.ChildRoute('/home', child: (context, args) => HomePage());

  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    context.read<HomeBlocCubit>().fetchingData();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(
      builder: (context, state) {
        if (state is HomeBlocLoadedState) {
          return _BodyHomePage(data: state.data);
        } else if (state is HomeBlocLoadingState) {
          return LoadingIndicator();
        } else if (state is HomeBlocErrorState) {
          return ErrorScreen(
            message: '${state.error}',
            fontSize: 20,
            iconData: state.iconData,
            retryButton: IconButton(
              icon: Icon(Icons.refresh_sharp, size: 35),
              onPressed: () {
                context.read<HomeBlocCubit>().fetchingData();
              },
            ),
          );
        }

        return Scaffold(
          body: Center(
              child: Text(kDebugMode ? "state not implemented $state" : "")),
        );
      },
    );
  }
}

class _BodyHomePage extends StatelessWidget {
  final List<Data> data;

  const _BodyHomePage({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBlocCubit, AuthBlocState>(
      listener: (context, state) {
        if (state is AuthBlocLoginState) {
          modular.Modular.to.pushReplacementNamed(LoginPage.route.name);
        }
      },
      child: Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
                onPressed: () {
                  context.read<AuthBlocCubit>().logout();
                },
                icon: Icon(Icons.logout))
          ],
        ),
        body: SafeArea(
          child: GridView.builder(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              shrinkWrap: true,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  childAspectRatio: 1,
                  mainAxisSpacing: 20,
                  mainAxisExtent: 250,
                  crossAxisSpacing: 20,
                  crossAxisCount: 2),
              itemCount: data.length,
              itemBuilder: (context, index) {
                final _data = data[index];
                return _CustomMovieCard(
                  data: _data,
                  onTap: () =>
                      modular.Modular.to.pushNamed('/home/movie/${_data.id}'),
                );
              }),
        ),
      ),
    );
  }
}

class _CustomMovieCard extends StatelessWidget {
  final Data data;
  final VoidCallback? onTap;

  const _CustomMovieCard({Key? key, required this.data, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.0))),
        child: Column(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.all(20),
                child: Image.network(
                  data.i.imageUrl,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 8, left: 20, right: 20),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Text(
                  data.l,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textDirection: TextDirection.ltr,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
