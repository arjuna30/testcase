import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/ui/home/home_page.dart';
import 'package:majootestcase/ui/login/login_page.dart';

class RegisterPage extends StatelessWidget {
  static final route =
      ChildRoute('/register', child: (context, args) => RegisterPage());

  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: _BodyRegisterPage(),
      ),
    );
  }
}

class _BodyRegisterPage extends StatefulWidget {
  const _BodyRegisterPage({Key? key}) : super(key: key);

  @override
  State<_BodyRegisterPage> createState() => _BodyRegisterPageState();
}

class _BodyRegisterPageState extends State<_BodyRegisterPage> {
  final _usernameController = TextController(initialValue: '');
  final _emailController = TextController(initialValue: '');
  final _passwordController = TextController(initialValue: '');
  final _confirmPasswordController = TextController(initialValue: '');
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  bool _isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBlocCubit, AuthBlocState>(
      listener: (context, state) {
        if (state is AuthBlocLoggedInState) {
          final snackBar = SnackBar(
              behavior: SnackBarBehavior.floating,
              content: Text('Berhasil Membuat Akun'));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
          Modular.to.pushReplacementNamed(HomePage.route.name);
        }
      },
      child: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 30, horizontal: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Daftar Akun Baru',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 30),
              _form(context),
              SizedBox(height: 24),
              Spacer(),
              CustomButton(
                text: 'Register',
                height: 60,
                onPressed: () {
                  if (formKey.currentState?.validate() == true) {
                    context.read<AuthBlocCubit>().register(
                          context,
                          username: _usernameController.value,
                          email: _emailController.value,
                          password: _passwordController.value,
                        );
                  }
                },
              ),
              _toLoginPage(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _form(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            hint: 'username',
            label: 'Username',
            validator: (val) {
              if (val != null && val.isEmpty) {
                return 'username required';
              }
            },
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = RegExp(r"([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})");
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          CustomTextFormField(
            context: context,
            label: 'Confirm Password',
            hint: 'confirm password',
            controller: _confirmPasswordController,
            isObscureText: _isObscurePassword,
            validator: (val) {
              if (val != null) {
                if (val != _passwordController.value)
                  return 'password missmatch';
              }
            },
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _toLoginPage() {
    return Center(
      child: TextButton(
        onPressed: () async {
          Modular.to.pushReplacementNamed(LoginPage.route.name);
        },
        child: RichText(
          text: TextSpan(
              text: 'Sudah Punya Akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(text: 'Masuk'),
              ]),
        ),
      ),
    );
  }
}
